Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: rtracklayer
Upstream-Contact: Michael Lawrence <michafla@gene.com>
Source: https://bioconductor.org/packages/rtracklayer/

Files: *
Copyright: © 2006-2016 Michael Lawrence, Vince Carey, Robert Gentleman
License: Artistic-2.0

Files: src/ucsc/*
Copyright: 2011-2014 The Regents of the University of California
License: all_use_kentlib
 *** TEXT FROM ORIGINAL KENT SOURCE LIB README ***
 This directory contains general purpose, web oriented,
 and computational biology oriented modules,  as well
 as some less general stuff associated with the Intronerator
 web site.  Initially this library was created by Jim
 Kent from 1999 to 2001.  Since then other programmers have
 also added to it.  Jim continues to make substantial
 contributions.
 .
 With the exception of the gifcomp.c module, all
 modules here are free for all use - public, private,
 or commercial. The gifcomp.c module contains code
 originally distributed by CompuServe under a similar
 "free for all" license.  However this uses the LZW
 patented by UniSys, which UniSys started enforcing
 several years after CompuServe released this module.
 The last I heard UniSys did not require a license
 for non-commercial purposes, but did require a
 license for commercial purposes. This may change
 in the future, though the patent is nearing it's
 expiration date in any case.  Commercial users
 are advised to either get a license for LZW, or
 to switch from GIF to PNG.

Files: src/ucsc/bits.*
       src/ucsc/common.*
       src/ucsc/dlist.*
       src/ucsc/dnaseq.*
       src/ucsc/dnautil.*
       src/ucsc/dystring.*
       src/ucsc/errAbort.*
       src/ucsc/hash.*
       src/ucsc/hmmstats.c
       src/ucsc/htmshell.h
       src/ucsc/linefile.*
       src/ucsc/localmem.*
       src/ucsc/memalloc.*
       src/ucsc/net.*
       src/ucsc/obscure.*
       src/ucsc/options.h
       src/ucsc/os*x.c
       src/ucsc/portable.h
       src/ucsc/sqlNum.*
       src/ucsc/wildcmp.c
       src/ucsc/cheapcgi.h
       src/ucsc/hmmstats.h
       src/ucsc/portimpl.h
       src/ucsc/sig.h
Copyright: 2000-2005 Jim Kent
License: all_use
 license is hereby granted for all use - public, private or commercial
Comment: The files
       src/ucsc/cheapcgi.h
       src/ucsc/hmmstats.h
       src/ucsc/portimpl.h
       src/ucsc/sig.h
 in this source download do contain the statement:
    Source code may be freely used
    for personal, academic, and non-profit purposes.  Commercial use
    permitted only by explicit agreement with Jim Kent (jim_kent@pacbell.net)
 but the author Jim Kent declared publicly here
    https://alioth-lists.debian.net/pipermail/debian-med-packaging/2016-May/042035.html
 that this is not the proper license but the license above applies.

Files: debian/*
Copyright: 2013-2016 Andreas Tille <tille@debian.org>
License: Artistic-2.0

License: Artistic-2.0
                         The "Artistic License"
 .
                                Preamble
 .
 1. You may make and give away verbatim copies of the source form of the
    Standard Version of this Package without restriction, provided that
    you duplicate all of the original copyright notices and associated
    disclaimers.
 .
 2. You may apply bug fixes, portability fixes and other modifications
    derived from the Public Domain or from the Copyright Holder.  A
    Package modified in such a way shall still be considered the Standard
    Version.
 .
 3. You may otherwise modify your copy of this Package in any way,
    provided that you insert a prominent notice in each changed file stating
    how and when you changed that file, and provided that you do at least
    ONE of the following:
 .
    a) place your modifications in the Public Domain or otherwise make them
    Freely Available, such as by posting said modifications to Usenet or
    an equivalent medium, or placing the modifications on a major archive
    site such as uunet.uu.net, or by allowing the Copyright Holder to include
    your modifications in the Standard Version of the Package.
 .
    b) use the modified Package only within your corporation or organization.
 .
    c) rename any non-standard executables so the names do not conflict
    with standard executables, which must also be provided, and provide
    a separate manual page for each non-standard executable that clearly
    documents how it differs from the Standard Version.
 .
    d) make other distribution arrangements with the Copyright Holder.
 .
 4. You may distribute the programs of this Package in object code or
    executable form, provided that you do at least ONE of the following:
 .
    a) distribute a Standard Version of the executables and library files,
    together with instructions (in the manual page or equivalent) on where
    to get the Standard Version.
 .
    b) accompany the distribution with the machine-readable source of
    the Package with your modifications.
 .
    c) give non-standard executables non-standard names, and clearly
    document the differences in manual pages (or equivalent), together
    with instructions on where to get the Standard Version.
 .
    d) make other distribution arrangements with the Copyright Holder.
 .
 5. You may charge a reasonable copying fee for any distribution of this
    Package.  You may charge any fee you choose for support of this Package.
    You may not charge a fee for this Package itself.  However, you may
    distribute this Package in aggregate with other (possibly commercial)
    programs as part of a larger (possibly commercial) software distribution
    provided that you do not advertise this Package as a product of your
    own.  You may embed this Package's interpreter within an executable of
    yours (by linking); this shall be construed as a mere form of
    aggregation, provided that the complete Standard Version of the
    interpreter is so embedded.
 .
 6. The scripts and library files supplied as input to or produced as
    output from the programs of this Package do not automatically fall under
    the copyright of this Package, but belong to whoever generated them, and
    may be sold commercially, and may be aggregated with this Package.  If
    such scripts or library files are aggregated with this Package via the
    so-called "undump" or "unexec" methods of producing a binary executable
    image, then distribution of such an image shall neither be construed as
    a distribution of this Package nor shall it fall under the restrictions
    of Paragraphs 3 and 4, provided that you do not represent such an
    executable image as a Standard Version of this Package.
 .
 7. C subroutines (or comparably compiled subroutines in other
    languages) supplied by you and linked into this Package in order to
    emulate subroutines and variables of the language defined by this
    Package shall not be considered part of this Package, but are the
    equivalent of input as in Paragraph 6, provided these subroutines do
    not change the language in any way that would cause it to fail the
    regression tests for the language.
 .
 8. Aggregation of this Package with a commercial distribution is always
    permitted provided that the use of this Package is embedded; that is,
    when no overt attempt is made to make this Package's interfaces visible
    to the end user of the commercial distribution.  Such use shall not be
    construed as a distribution of this Package.
 .
 9. The name of the Copyright Holder may not be used to endorse or promote
    products derived from this software without specific prior written permission.
 .
 10. THIS PACKAGE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
    IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
    WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.

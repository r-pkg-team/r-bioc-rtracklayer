Source: r-bioc-rtracklayer
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>,
           Charles Plessy <plessy@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-bioc-genomicranges,
               r-cran-xml,
               r-bioc-biocgenerics,
               r-bioc-s4vectors,
               r-bioc-iranges,
               r-bioc-xvector,
               r-bioc-genomeinfodb,
               r-bioc-biostrings,
               r-bioc-zlibbioc,
               r-cran-curl,
               r-cran-httr,
               r-bioc-rsamtools,
               r-bioc-genomicalignments,
               r-bioc-biocio,
               r-cran-restfulr,
               architecture-is-64-bit,
               libcurl4-openssl-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-rtracklayer
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-rtracklayer.git
Homepage: https://bioconductor.org/packages/rtracklayer/
Rules-Requires-Root: no

Package: r-bioc-rtracklayer
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R interface to genome browsers and their annotation tracks
 Extensible framework for interacting with multiple genome browsers
 (currently UCSC built-in) and manipulating annotation tracks in various
 formats (currently GFF, BED, bedGraph, BED15, WIG, BigWig and 2bit
 built-in). The user may export/import tracks to/from the supported
 browsers, as well as query and modify the browser state, such as the
 current viewport.

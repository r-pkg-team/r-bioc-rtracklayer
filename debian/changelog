r-bioc-rtracklayer (1.66.0-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 13 Jan 2025 15:21:09 +0100

r-bioc-rtracklayer (1.66.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 08 Nov 2024 11:27:10 +0100

r-bioc-rtracklayer (1.64.0-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 18 Aug 2024 11:45:47 +0200

r-bioc-rtracklayer (1.64.0-1~0exp1) experimental; urgency=medium

  * Team upload.
  * d/{,tests/}control: bump minimum versions of r-bioc-* packages to
    bioc-3.19+ versions

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 23 Jul 2024 12:58:01 +0200

r-bioc-rtracklayer (1.64.0-1~0exp) experimental; urgency=medium

  * Team upload.
  * New upstream version. Closes: #1071347
  * dh-update-R to update Build-Depends (routine-update)
  * Set upstream metadata fields: Archive.
  * New build-dep: libcurl4-openssl-dev
  * d/control: Skip building on 32-bit systems.

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 09 Jul 2024 23:32:07 +0200

r-bioc-rtracklayer (1.62.0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 30 Nov 2023 18:23:42 +0100

r-bioc-rtracklayer (1.60.1-1) unstable; urgency=medium

  * New upstream version
  * Patch harder to tell that BSgenome.Hsapiens.UCSC.hg19 is not available

 -- Andreas Tille <tille@debian.org>  Fri, 25 Aug 2023 12:52:38 +0200

r-bioc-rtracklayer (1.60.0-1) unstable; urgency=medium

  [ Andreas Tille ]
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)
  * Try running upstream test suite as autopkgtest

 -- Charles Plessy <plessy@debian.org>  Thu, 27 Jul 2023 00:09:12 +0000

r-bioc-rtracklayer (1.58.0-1) unstable; urgency=medium

  * Update link to alioth list
  * Disable reprotest
  * New upstream version
  * Reduce piuparts noise in transitions (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 21 Nov 2022 14:21:59 +0100

r-bioc-rtracklayer (1.56.1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 27 Jun 2022 14:11:46 +0200

r-bioc-rtracklayer (1.56.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 16 May 2022 07:30:41 +0200

r-bioc-rtracklayer (1.54.0-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 25 Nov 2021 14:57:54 +0100

r-bioc-rtracklayer (1.52.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (3) (routine-update)
  * Build-Depends: r-bioc-biocio, r-cran-restfulr
  * Not testing since non-packaged data is needed

 -- Andreas Tille <tille@debian.org>  Fri, 10 Sep 2021 15:22:48 +0200

r-bioc-rtracklayer (1.50.0-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 13 (routine-update)
  * No tab in license text (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 03 Nov 2020 13:30:01 +0100

r-bioc-rtracklayer (1.48.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Wed, 20 May 2020 11:17:28 +0200

r-bioc-rtracklayer (1.46.0-2) unstable; urgency=medium

  * Team upload.
  * Add patch to fix autopkgtest.
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Tue, 17 Mar 2020 14:35:47 +0100

r-bioc-rtracklayer (1.46.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.4.1

 -- Dylan Aïssi <daissi@debian.org>  Mon, 11 Nov 2019 23:52:24 +0100

r-bioc-rtracklayer (1.44.4-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 13 Sep 2019 08:27:31 +0200

r-bioc-rtracklayer (1.44.3-1) unstable; urgency=medium

  * New upstream version
  * Fixed debian/watch for BioConductor

 -- Andreas Tille <tille@debian.org>  Fri, 30 Aug 2019 11:37:24 +0200

r-bioc-rtracklayer (1.44.2-1) unstable; urgency=medium

  * New upstream version
  * Drop note about no testing in debian/tests/control_
  * debhelper-compat 12

 -- Andreas Tille <tille@debian.org>  Sat, 27 Jul 2019 19:40:00 +0200

r-bioc-rtracklayer (1.44.0-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.4.0
  * Remove trailing whitespace in debian/copyright

 -- Andreas Tille <tille@debian.org>  Fri, 19 Jul 2019 10:00:24 +0200

r-bioc-rtracklayer (1.42.1-2) unstable; urgency=medium

  * Team upload.
  * dh-update-R: Remove build-dep libgdal-dev and add r-bioc-zlibbioc.
  * Standards-Version: 4.3.0

 -- Dylan Aïssi <daissi@debian.org>  Thu, 10 Jan 2019 08:18:08 +0100

r-bioc-rtracklayer (1.42.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Thu, 29 Nov 2018 08:13:03 +0100

r-bioc-rtracklayer (1.42.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
    Closes: #913527
  * dh-update-R to update Build-Depends

 -- Dylan Aïssi <daissi@debian.org>  Mon, 12 Nov 2018 07:51:25 +0100

r-bioc-rtracklayer (1.40.6-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Wed, 05 Sep 2018 21:12:06 +0200

r-bioc-rtracklayer (1.40.5-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 23 Aug 2018 23:14:44 +0200

r-bioc-rtracklayer (1.40.4-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.2.0

 -- Andreas Tille <tille@debian.org>  Mon, 13 Aug 2018 09:35:55 +0200

r-bioc-rtracklayer (1.40.3-1) unstable; urgency=medium

  * Rebuild for R 3.5 transition
  * New upstream version
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Tue, 05 Jun 2018 11:25:57 +0200

r-bioc-rtracklayer (1.40.2-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sat, 12 May 2018 15:10:46 +0200

r-bioc-rtracklayer (1.40.1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 07 May 2018 08:18:44 +0200

r-bioc-rtracklayer (1.40.0-2) unstable; urgency=medium

  * Fix Maintainer and Vcs-fields

 -- Andreas Tille <tille@debian.org>  Sun, 06 May 2018 07:52:04 +0200

r-bioc-rtracklayer (1.40.0-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.4

 -- Andreas Tille <tille@debian.org>  Thu, 03 May 2018 16:09:57 +0200

r-bioc-rtracklayer (1.38.3-1) unstable; urgency=medium

  * New upstream version
  * Preparation of data package moved from SVN to Git
  * Standards-Version: 4.1.3
  * debhelper 11

 -- Andreas Tille <tille@debian.org>  Mon, 12 Mar 2018 20:41:34 +0100

r-bioc-rtracklayer (1.38.0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 09 Nov 2017 09:06:32 +0100

r-bioc-rtracklayer (1.36.6-1) unstable; urgency=medium

  * New upstream version
  * Secure URI in watch file

 -- Andreas Tille <tille@debian.org>  Sun, 22 Oct 2017 19:25:43 +0200

r-bioc-rtracklayer (1.36.4-2) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * Standards-Version: 4.1.1
  * Testsuite: autopkgtest-pkg-r

 -- Andreas Tille <tille@debian.org>  Mon, 02 Oct 2017 23:17:00 +0200

r-bioc-rtracklayer (1.36.4-1) unstable; urgency=medium

  * New upstream version
    Closes: #871333
  * Standards-Version: 4.0.1 (no changes needed)
  * Remove unused lintian override

 -- Andreas Tille <tille@debian.org>  Wed, 23 Aug 2017 19:43:41 +0200

r-bioc-rtracklayer (1.34.1-1) unstable; urgency=medium

  * New upstream version
    Closes: #846937
  * debhelper 10
  * d/watch: version=4
  * Remove unused lintian override
  * New Build-Depends: pkg-config

 -- Andreas Tille <tille@debian.org>  Sun, 04 Dec 2016 21:53:17 +0100

r-bioc-rtracklayer (1.34.0-2) unstable; urgency=medium

  * Rebuild against libssl1.0.2 from unstable

 -- Andreas Tille <tille@debian.org>  Thu, 27 Oct 2016 13:40:18 +0200

r-bioc-rtracklayer (1.34.0-1) unstable; urgency=medium

  * New upstream version
  * Convert to dh-r
  * Generic BioConductor homepage

 -- Andreas Tille <tille@debian.org>  Thu, 27 Oct 2016 11:37:57 +0200

r-bioc-rtracklayer (1.32.2-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 16 Aug 2016 15:36:09 +0200

r-bioc-rtracklayer (1.32.1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Mon, 27 Jun 2016 12:04:24 +0200

r-bioc-rtracklayer (1.32.0-1) unstable; urgency=medium

  * New upstream version
  * cme fix dpkg-control
  * Add missing Build-Depends: libssl-dev
  * debian/copyright: Clarify license of files in src/ucsc/*
    Closes: #807580

 -- Andreas Tille <tille@debian.org>  Mon, 09 May 2016 12:46:31 +0200

r-bioc-rtracklayer (1.30.1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 06 Nov 2015 09:01:53 +0100

r-bioc-rtracklayer (1.28.10-1) unstable; urgency=medium

  * New upstream version
  * Adapt watch file
  * lintian-overrides for package-contains-timestamped-gzip

 -- Andreas Tille <tille@debian.org>  Wed, 23 Sep 2015 21:32:13 +0200

r-bioc-rtracklayer (1.28.6-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 17 Jul 2015 21:08:35 +0200

r-bioc-rtracklayer (1.28.5-2) unstable; urgency=medium

  * Upload to unstable instead of experimental

 -- Andreas Tille <tille@debian.org>  Sun, 28 Jun 2015 07:37:34 +0200

r-bioc-rtracklayer (1.28.5-1) experimental; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sat, 27 Jun 2015 23:09:08 +0200

r-bioc-rtracklayer (1.26.0-1) experimental; urgency=medium

  * New upstream version
  + cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Tue, 21 Oct 2014 20:34:34 +0200

r-bioc-rtracklayer (1.24.2-1) unstable; urgency=medium

  * New upstream version
  * Moved debian/upstream to debian/upstream/metadata
  * (Build-)Depends: r-bioc-genomicalignments
  * Drop autopkgtest that would need 800MB data package
    r-bioc-bsgenome.hsapiens.ucsc.hg19 to run successfully.
    Closes: #735543

 -- Andreas Tille <tille@debian.org>  Fri, 08 Aug 2014 13:37:40 +0200

r-bioc-rtracklayer (1.22.0-1) unstable; urgency=low

  * Initial release (closes: #733283).

 -- Andreas Tille <tille@debian.org>  Sat, 28 Dec 2013 00:57:59 +0100
